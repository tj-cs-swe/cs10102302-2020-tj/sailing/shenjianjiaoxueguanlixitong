from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
notice = Table('notice', post_meta,
    Column('Notice_ID', Integer, primary_key=True),
    Column('Course', String(length=32)),
    Column('Topic', Text),
    Column('Body', Text),
    Column('UserIdentity', Integer),
    Column('User_ID', String(length=7)),
    Column('UserName', String(length=16)),
    Column('Timestamp', DateTime),
    Column('IsPosted', Boolean),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['notice'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['notice'].drop()
