import click
import os
from app import app, db
from app.models import Problem
from app.OJ_Module.judge_code import PROBLEM_DIR

@app.cli.command()
def insert():
    # already built up
    # db.create_all()
    
    Filenames = os.listdir(PROBLEM_DIR)

    for filename in Filenames:
        problem = Problem(Problem_Name = filename + "(题库)", Teacher_ID = "1001")
        db.session.add(problem)
    
    db.session.commit()

    click.echo('Done.')

@app.cli.command()  # ?????
@click.option('--drop', is_flag=True, help='Create after drop.')  # ????
def initdb(drop):
    """Initialize the database."""
    if drop:  # ?????????
        db.drop_all()
    db.create_all()
    click.echo('Initialized database.')  # ??????